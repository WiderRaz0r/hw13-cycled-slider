const imagesArr = [...document.querySelectorAll(`img`)];
const buttonStop = document.querySelector(`.button-stop`)
const buttonStart = document.querySelector(`.button-start`)

let currentImg = 0;
let prevImg = 0;
let intervalId = setInterval(slider,3000);

function slider(){
    imagesArr[prevImg].classList.remove(`image-to-show-active`);
    imagesArr[prevImg].classList.add(`image-to-show`);
    currentImg++;
    if(currentImg > imagesArr.length - 1) currentImg = 0;
    imagesArr[currentImg].classList.remove(`image-to-show`);
    imagesArr[prevImg].classList.add(`image-to-show-active`);
    prevImg = currentImg;
}

function stop() {
        clearInterval(intervalId);
}

function start(){
        let intervalId = setInterval(slider, 3000);
}

buttonStop.addEventListener(`click`, stop)
buttonStart.addEventListener(`click`, start)
